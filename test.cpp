#include <cassert>
#include "liba.hpp"

void test_version() {
	assert(liba::version() == "1.0");
}

int main(int argc, char * argv[]) {
	test_version();
	return 0;
}
